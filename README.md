# enviroData



## Installation

To install `enviroData` you will need to have the R package `remotes` installed. 

In R:
```{r} 
install.packages("remotes") 
```

```{r} 
install.packages("git2r")
remotes::install_git("https://gitlab.com/michaelfolkes/enviroData")
```

Not all functions have been fully tested. 

Once the package is installed, if using Rstudio, you will find some basic information in the user guides area of the package documentation.



## Demo scripts

The function `writeScript` will make it easier to start things. The help file has an example:
```
?enviroData::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "enviroData")
```

To save and open a specific demo script (this one named "demo.R"):
```
writeScript("demo")

```

Here is how to open a copy of the script that shows how to download and combine historical and real time Water Survey Canada's (WSC/NHS) river discharge date:
```
writeScript("demo_hydrological_data.R")

```


Here is how to open a copy of the script that downloads the OSCAR current velocity data:
```
writeScript("demo_oscarv2_dap4_download.R")

```
Here is how to open a copy of the script that post-processes the OSCAR current velocity data:
```
writeScript("demo_oscarv2_dap4_process.R")

```
