

# comments ----------------------------------------------------------------

#author: Michael Folkes
#"Fri Dec 16 13:28:42 2022"

#this script downloads and post-processes the NOAA OISST v2. data





# packages ----------------------------------------------------------------

require(enviroData)
# require(geosphere)
# require(PBSmapping)



# function defs -----------------------------------------------------------




# setup -------------------------------------------------------------------


#this config file includes any number of named user.paths for pointing to input and output folders:
#this file is user specific, as such it is in the .gitignore and won't be followed by git
settings <- yaml::read_yaml("user.settings.config")


setwd(settings$path.wd)


# data: download sst --------------------------------------------------------

#these url's are for looking around and learning data source. The download function below has the necessary url
#get latest monthly data file and convert to running means:
#https://psl.noaa.gov/data/gridded/data.noaa.oisst.v2.html
#link to list of files:
#https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2

#daily data files are more timely in their updates:
#https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2.highres/
#for example:
#https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2.highres/sst.day.mean.2023.nc

#land-sea mask file, only need once as it's never revised:
#https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2/lsmask.nc
#monthly mean SST:
#https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2/sst.mnmean.nc



filename <- paste("sst.mnmean_Accessed", Sys.Date(), ".nc", sep="_")
filepath <- paste(settings$path.sst, filename, sep="/")
download.file(url = "https://downloads.psl.noaa.gov/Datasets/noaa.oisst.v2/sst.mnmean.nc", destfile = filepath, mode = "wb")

# data: import sst --------------------------------------------------------

filepath <- list.files(path = settings$path.sst,pattern = ".*mnmean.*nc$", full.names = T, ignore.case = T)
filepath <- list.files(path = settings$path.sst,pattern = "nc$", full.names = T, ignore.case = T)
filepath
filepath <- filepath[3]

#data.sst <- enviroData::importOISST(filepath, lsmask.filepath = settings$lsmask.filepath)
data.sst <- importOISST(filepath, lsmask.filepath = settings$lsmask.highres.filepath)
str(data.sst)
View(data.sst)
data.sst$period <- paste(data.sst$year.vec, sprintf("%02d", data.sst$month.vec), sep = "-")

data.sst$data.foranalysis <- aperm(data.sst$data.foranalysis, perm = c(2,1,3))
dimnames(data.sst$data.foranalysis) <- list(lat=data.sst$latitude.vec, lon=data.sst$longitude.vec, period=data.sst$period)




# calc grid mean ----------------------------------------------------------
#not done any more

# averaging:grid ----------------------------------------------------------

#this defines the 3 coastal polygons that will each have gridded monthly averages calculated:
#all of this serves to revise the corners of the 3 polygons above so that the long edge (north to south) of each is about 500km (ie 500km * 1000m/km), then draw tangents, each 150km long (d=150km*1000m/km) that are 90deg perpendicular to the coastline (which has a bearing of about 325deg - not sure how I estimated that!)
#the anchor points are NW corner of Haida Gwaii and NW corner of Vanc Is.



# define polygons for grid averaging ----------------------------------------------------------

polys <- read.table(col.names = c("list.ind", "PID", "POS", "Y","X", "revised", "comment"), fill = TRUE, text="
1 1 1 58.40089 -136.0884 TRUE
1 1 2 54.25000 -133.0000 FALSE
1 1 3 53.46258 -134.8502 TRUE
1 1 4 57.89226 -138.4471 FALSE
1 2 1 54.25000 -133.0000 TRUE
1 2 2 50.75000 -128.2500 FALSE
1 2 3 49.96380 -129.9626 TRUE
1 2 4 53.46258 -134.8502 TRUE
1 3 1 50.75000 -128.2500 TRUE
1 3 2 48.00000 -124.0000 FALSE
1 3 3 47.21459 -125.6221 TRUE
1 3 4 49.96380 -129.9626 TRUE
# 4 4 1 50.00000 -140.0000 FALSE 'polygon 4 is for nov-dec sst From Blackbourn model'
# 4 4 2 45.00000 -140.0000 FALSE
# 4 4 3 45.00000 -150.0000 FALSE
# 4 4 4 50.00000 -150.0000 FALSE
# 5 5 1 59.00000 -143.5000 FALSE 'polygon 5 is march oscurs'
# 5 5 2 59.00000 -146.5000 FALSE 'polygon 5 is march oscurs'
# 5 5 3 56.00000 -146.5000 FALSE 'polygon 5 is march oscurs'
# 5 5 4 56.00000 -143.5000 FALSE 'polygon 5 is march oscurs'
# 6 6 1 46.50000 -138.5000 FALSE 'polygon 6 is may oscurs'
# 6 6 2 46.50000 -141.5000 FALSE 'polygon 6 is may oscurs'
# 6 6 3 43.50000 -141.5000 FALSE 'polygon 6 is may oscurs'
# 6 6 4 43.50000 -138.5000 FALSE 'polygon 6 is may oscurs'
")

polys.list <- split(polys, polys$list.ind)
polys.list <- lapply(polys.list, list)
#The includeBdry variable is an argument for PBSmapping::findPolys
#if 0 then exclude grid points on the polygon boundaries, if NULL then include
includeBdry <- rep(0, length(polys.list))

#polygons 1:3 are defined in one list, this way the other polygons, which overlap with them, can be calculated without causing problems later (ie each is evaluated separately)
polys.list <- Map(function(x,y) c(includeBdry=x,polys=y), as.list(includeBdry), polys.list)
polys.list

# link grid data to a polygon ------------------------------------------
polys.list[[1]]$period <- format(data.sst$ncdates, "%Y-%m")
# polys.list[[2]]$period <- paste(data.sst$year.vec, "nonWinter", sep="-")
# polys.list[[2]]$period[data.sst$month.vec %in% 11:12] <- paste(data.sst$year.vec[data.sst$month.vec %in% 11:12], "Winter", sep="-")

lat <- data.sst$latitude.vec
lon <- data.sst$longitude.vec

#normal set of polygons & Blackbourn polygons too:
data.gridgroup.list <- lapply(polys.list, function(x) defineGridgroups(lat = lat,lon = lon, period = x$period, polys = x$polys, includeBdry = x$includeBdry))



# data:grid average -------------------------------------------------------
# calc mean of sst by polygon and period, which in this case is a year-month (or however the period is defined)

system.time(
  data.gridmean.list <- lapply(data.gridgroup.list, function(data.gridgroup){
    calcArraymean(data.sst$data.foranalysis,arr.group = data.gridgroup$data.gridgroup.arr, labels=data.gridgroup$gridgroup)
  })
)



# this adds to the long data frame the value of the variable (ie sst)
data.gridmean.list <- lapply(data.gridmean.list, function(data.gridmean) {
  data.gridmean$data.long$variable <- "sst"
  data.gridmean
})


#remove polygon 0 from second list that has polygon 4
# data.gridmean.list[[2]]$data.long <- data.gridmean.list[[2]]$data.long[data.gridmean.list[[2]]$data.long$polygon==4,]
# data.gridmean.list[[2]]$data.long <- data.gridmean.list[[2]]$data.long[grep("-Winter", data.gridmean.list[[2]]$data.long$period),]
#
# data.gridmean.list[[2]]$data.long$month <- "Winter"
# data.gridmean.list[[2]]$data.long$year <- data.gridmean.list[[2]]$data.long$year+1
#
# str(data.gridmean.list[[2]])

data.gridmean <- do.call(rbind, lapply(data.gridmean.list, "[[", "data.long"))
str(data.gridmean)
unique(data.gridmean$polygon)

# data:export -------------------------------------------------------------


#this is the file to be used in forecasting timing/diversion:
filename <- paste0("oisst_gridmean_",Sys.Date(), ".rds" )
filepath <- paste(settings$path.sst, filename, sep="/")
saveRDS(object = data.gridmean, file = filepath)



# plotting: time series of sst --------------------------------------------

data.tmp.wide <- data.gridmean.list[[1]]$data.wide
data.tmp.wide <- data.tmp.wide[data.tmp.wide$month %in% 1:5,]

require(ggplot2)
dev.new()
ggplot(data=data.tmp.wide, aes(month, value, col=as.factor(polygon)))+
	geom_line()+
	scale_x_continuous(breaks = pretty(data.tmp.wide$month))+
	facet_wrap(vars(year))


ggplot(data=data.tmp.wide, aes(year, value))+
	geom_line()+
	#scale_x_continuous(breaks = pretty(data.tmp.wide$month))+
	facet_grid(vars(month), vars(polygon), scales = "free_y")



ggplot(data=data.tmp.wide, aes(as.factor(month), value))+
	geom_boxplot(notch = TRUE)+
	#scale_x_continuous(breaks = pretty(data.tmp.wide$month))+
	facet_wrap(vars(polygon))

ggplot(data=data.tmp.wide, aes(as.factor(polygon), value))+
  geom_boxplot(notch = TRUE)+
  #scale_x_continuous(breaks = pretty(data.tmp.wide$month))+
  facet_wrap(vars(month))




