library(enviroData)

rm(list=ls())



# setup fake data ---------------------------------------------------------

datetime <- seq(as.POSIXct("2024-5-1 12:00", tz="UTC"), as.POSIXct("2024-5-5 12:00", tz="UTC"), by="hour")
data <- expand.grid(datetime=datetime, station=1:2)
data$hour <- as.POSIXlt(data$datetime)$hour
data

x <- data$hour

a <- 8 #amplitude
b <- 2*pi/24 #period =2*pi/b
c <- 12 #phase shift
d <- 10 #vertical shift
data$y <- y <- a*sin(b*(x+c))+d
data$value <- y2 <- y+rnorm(length(y))*5
require(ggplot2)
ggplot(data=data, aes(datetime, y))+
  geom_point()+
  geom_line(aes(y=y2))+
  facet_wrap(vars(station))


data.list <- split(data, data$station)


# identify anomalous values ---------------------------------------------

# this defines a generic set of probability levels for each station and metric that will be used to calculate the upper quantile that will then define the upper threshold for data to be flagged
criteria.list <- buildCriteriaList(prob = pnorm(1))
criteria.list
#instead of relying on probability levels, hard thresholds can be pre-set:
# criteria.list$laggingdiff$criteria <- list(threshold = 0.8)
# criteria.list$leadingdiff$criteria <- list(threshold = 0.8)
# criteria.list$rollmeandiff.left$criteria <- list(threshold = 1.5)
# criteria.list$rollmeandiff.right$criteria <- list(threshold = 1.5)
# criteria.list$rollSD.center$criteria <- list(threshold = 2)
# criteria.list$rollSD.left$criteria <- list(threshold = 2)
# criteria.list$rollSD.right$criteria <- list(threshold = 2)

criteria.list$rangelower$criteria <- list(prob=NA, threshold = -0.1)



#filter out data en-masse, this method will likely remove neighbours to outliers:
data.filtered.list <- runFilter(datalist = data.list, criteria.list = criteria.list, iterate=NULL)

#if the criteria are listed in the 'iterate' argument, then the function will go through them individually in the order listed. it will evaluate outliers and remove them, one method at a time. this is less likely to remove neighbours to outliers
#can change the order of filters to see if impacts output
data.filtered.list <- runFilter(datalist = data.list, criteria.list = criteria.list, iterate=c("rangelower", "rangeupper", "laggingdiff", "leadingdiff",  'rollmeandiff.left', 'rollmeandiff.right','rollSD.center', 'rollSD.left', 'rollSD.right'))


str(data.filtered.list[[1]]$method.list)
data.filtered.df <- do.call(rbind, lapply(data.filtered.list, "[[", "data"))

View(data.filtered.df)

data.filtered.list$method.list$rangelower$criteria
#get a sense of the threshold used, based on estimated q at 3 SD's.
res <- lapply(data.filtered.list, function(x) {
  tmp <- lapply(x$method.list, function(x) as.data.frame(x$criteria))
  do.call(rbind, tmp)
})
res <- lapply(res, function(x) data.frame(method=rownames(x), x))
res <- Map(data.frame, res, stationID=names(res))
res <- do.call(rbind, res)
rownames(res) <- NULL
res
reshape(res, direction = 'wide',drop = "prob", idvar='stationID', timevar = "method")


#subset for any anomalies having TRUE result
cols <- c('rangelower','rangeupper', 'laggingdiff', 'leadingdiff', 'rollmeandiff.left', 'rollmeandiff.right', 'rollSD.center', 'rollSD.left', 'rollSD.right')
row.ind <- which(rowSums(data.filtered.df[,cols],na.rm = TRUE)>=1)
length(row.ind)


data.filtered.df$year <- as.integer(format(data.filtered.df$datetime, "%Y"))
data.anom <- data.filtered.df[ row.ind,]
nrow(data.anom)

table(data.filtered.df$station, data.filtered.df$year)

idvar <- c("station", "datetime", "year", "value")
varying <- read.table(header=TRUE, text="
measure group
rangelower range
rangeupper range
laggingdiff laglead
leadingdiff laglead
rollmeandiff.left rollmean
rollmeandiff.right rollmean
rollSD.center sd
rollSD.left sd
rollSD.right sd
")


data.filtered.df.long <- apply(varying,1, function(x){
  x <- as.list(x)
  data.tmp <- data.filtered.df[!is.na(data.filtered.df[,x$measure]) & data.filtered.df[,x$measure]==TRUE,idvar]
  if(nrow(data.tmp)>0){
    data.tmp$measure <- x$measure
    data.tmp$group <- x$group
  }
  data.tmp

})
str(data.filtered.df)
str(data.filtered.df.long)
data.filtered.df.long <- do.call(rbind, data.filtered.df.long)

require(ggplot2)
size <- 4

ggplot(data.filtered.df,aes(datetime, value))+
  geom_line(size=0.05)+
  geom_point(data=data.filtered.df.long, aes(col=group), shape=16, size=size, alpha=0.5)+
  scale_x_datetime(date_breaks = "1 days", date_labels= "%b-%d") +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))+
  xlab("GMT date standarized to 2022")+
  facet_wrap(vars(station))+
  guides(color = guide_legend(override.aes = list(size = 3, shape=16)))

