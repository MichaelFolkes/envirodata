
#' Build lists of urls etc for downloading OSCAR V2
#'
#' @param series.df
#'
#' @return
#' @export
#'
#' @examples
buildOSCARdownloads <- function(series.df){

  series.df$date.start <- as.Date(series.df$date.start)
  series.df$date.end <- as.Date(series.df$date.end)

  series.df$search.url.prefix <- "https://cmr.earthdata.nasa.gov/virtual-directory/collections"
  series.df$search.url <- paste(series.df$search.url.prefix,series.df$series.folder, "temporal", sep = "/")
  series.df$opendap.url.prefix <- "https://opendap.earthdata.nasa.gov/collections"

  if(is.na(series.df$date.end[3]))  series.df$date.end[3] <- Sys.Date()

  #search the website to confirm files exist (takes >hour)
  # files.cloud <- lapply(1:nrow(series.df), function(row.ind){
  # 	x <- as.list(series.df[row.ind,])
  # 	files.cloud <- listOSCARv2file(url.str = x$search.url, dates = seq(x$date.start, x$date.end, by="day"))
  # 	files.cloud <- lapply(files.cloud, function(x2){c(x2,x)})
  # 	files.cloud
  # })

  #build the list of online files without confirming they exist:
  files.cloud <- lapply(1:nrow(series.df), function(row.ind){
    x <- as.list(series.df[row.ind,])

    dates <- seq(x$date.start, x$date.end, by="day")
    dates <- gsub("-", "/", dates)
    filename <- paste0("oscar_currents_", x$series.name,"_", gsub("/", "", dates), ".nc")

    month <- as.integer(substr(dates, 6,7))
    x2 <- data.frame(date=dates, month=month, filename=filename, x)
    x2 <- apply(x2, 1, function(x){
      type.convert(as.list(x), as.is=TRUE)
    })
    x2
  })

  files.cloud <- unlist(files.cloud, recursive = FALSE)


  files.cloud <- lapply(files.cloud, function(x){
    url.path <- paste(x$opendap.url.prefix, x$series.folder, "granules", sep="/")
    filename.prefix <- tools::file_path_sans_ext(x$filename)
    x$filename.txt <- paste0(filename.prefix, ".txt")
    url.tmp <- buildURLoscarV2(url.path = url.path, filename.prefix = filename.prefix,  time.length = 1)
    x$opendap.url <- url.tmp$opendap.url
    x$data.url <- url.tmp$data.url
    x
  })

  names(files.cloud) <- unlist(lapply(files.cloud, "[[", "filename"))
  files.cloud

}#END buildOSCARdownloads




#' Title
#'
#' @param url.path
#' @param filename.prefix
#' @param file.prefix
#' @param filename.suffix
#' @param time.length
#' @param lat1
#' @param lat2
#' @param long1
#' @param long2
#'
#'
#' @details Latitude ranges 80 degrees north to 80 degrees south in 1/3 degree intervals. Including the equator, a range of 161 degrees * 3 points per degree equals 481 indices (`length(seq(80, -80, by=-1/3))`). Index 0 is 80deg north, and 80deg south (i.e. -80) is index 480. Longitude ranges 20 degrees east to 420 degrees east in 1/3 degree intervals. Including the prime meridian (0 degrees), it is a range of 401 degrees * 3 points per degree equals 1201 indices `length(seq(20, 420, by=1/3))`. Index 0 is 20deg east, and 420deg east is index 1200.
#'
#' @return
#' @export
#'
#' @examples
buildURLoscar <- function(url.path = "http://podaac-opendap.jpl.nasa.gov:80/opendap/allData/oscar/preview/L4/oscar_third_deg/", filename.prefix,  filename.extension = ".nc.gz.ascii?", time.length, lat1=60, lat2=40, long1=180, long2=240){
  lat1.ind <-(80-lat1)*3
  lat2.ind <-(80-lat2)*3
  latstr <- paste(lat1.ind,1,lat2.ind, sep = ":")
  long1.ind <-(long1-20)*3
  long2.ind <-(long2-20)*3
  longstr <- paste(long1.ind,1,long2.ind, sep = ":")

  vars.df <- data.frame(matrix(ncol = 4, byrow=TRUE,
                               c("TRUE", "maps", "time", paste0("0:1:", time.length-1),
                                 "TRUE", "maps", "depth", "0:1:0",
                                 "TRUE", "maps", "latitude", latstr,
                                 "TRUE", "maps", "longitude", longstr,
                                 "TRUE", "array", "u", NA,
                                 "TRUE", "array", "v", NA
                               )
  ), stringsAsFactors = FALSE)
  colnames(vars.df) <- c("include", "var.type", "var.name", "var.index")

  var.indices <- apply(vars.df, 1, FUN = function(x, vars.df){
    if(x['include']==TRUE & x['var.type']=="maps") {
      paste0(x['var.name'], "[", x['var.index'], "],")
    }else if(x['include']==TRUE & x['var.type']=="array"){
      var.index.tmp <- vars.df$var.index[vars.df$include==TRUE & vars.df$var.type=="maps"]
      index.combined <- paste0("[", var.index.tmp , "]", collapse = "")
      paste0(x['var.name'], index.combined, ",")

    }

  }, vars.df)
  var.indices <- paste(var.indices, collapse = "")

  #remove trailing comma if it exists:
  var.indices <- sub(",$", "", var.indices)

  url.str <- paste0(url.path, filename.prefix, filename.extension, var.indices   )
  return(url.str)

}#END buildURLoscar



#' buildURLoscarV2
#'
#' @param url.path
#' @param filename.prefix
#' @param filename.extension
#' @param dap.str
#' @param time.length
#' @param lat1
#' @param lat2
#' @param long1
#' @param long2
#' @param grid.latitide
#' @param grid.longitude
#'
#' @return
#' @export
#'
#' @examples
buildURLoscarV2 <- function(url.path = "https://opendap.earthdata.nasa.gov/collections/C2102958977-POCLOUD/granules", filename.prefix, filename.extension=".dap.nc4",  dap.str = "?dap4.ce=", time.length, lat1=40, lat2=60, long1=180, long2=240, grid.latitide=seq(-89.75,89.75, by=.25), grid.longitude=seq(0,359.75, by=.25)){

  #subtract 1 as vector index starts at zero
  lat1.ind <- which(grid.latitide %in% lat1)-1
  lat2.ind <- which(grid.latitide %in% lat2)-1
  latstr <- paste(lat1.ind,1,lat2.ind, sep = ":")

  long1.ind <- which(grid.longitude %in% long1)-1
  long2.ind <- which(grid.longitude %in% long2)-1
  longstr <- paste(long1.ind,1,long2.ind, sep = ":")

  vars.df <- data.frame(matrix(ncol = 4, byrow=TRUE,
                               c("TRUE", "maps", "/lon", longstr,
                                 "TRUE", "maps", "/time", paste0("0:1:", time.length-1),
                                 "TRUE", "array", "/u", NA,
                                 "TRUE", "maps", "/lat", latstr,
                                 "TRUE", "array", "/v", NA
                               )
  ))
  colnames(vars.df) <- c("include", "var.type", "var.name", "var.index")

  var.indices <- apply(vars.df, 1, FUN = function(x, vars.df){
    if(x['include']==TRUE & x['var.type']=="maps") {
      paste0(x['var.name'], "%5B", x['var.index'], "%5D;")
    }else if(x['include']==TRUE & x['var.type']=="array"){
      #var.index.tmp <- vars.df$var.index[vars.df$include==TRUE & vars.df$var.type=="maps"]
      var.index.tmp <- vars.df$var.index[match(c("/time", "/lon", "/lat"), vars.df$var.name)]
      index.combined <- paste0("%5B", var.index.tmp , "%5D", collapse = "")
      paste0(x['var.name'], index.combined, ";")

    }

  }, vars.df)
  var.indices <- paste(var.indices, collapse = "")

  #remove trailing semicolon if it exists:
  var.indices <- sub(";$", "", var.indices)

  var.indices.dap3 <- gsub("%5B", "[", var.indices)
  var.indices.dap3 <- gsub("%5D", "]", var.indices.dap3)
  data.url <- paste0(url.path,"/", filename.prefix, dap.str, var.indices.dap3)

  opendap.url <- paste0(url.path,"/", filename.prefix, filename.extension, dap.str, var.indices)
  return(list(opendap.url=opendap.url, data.url=data.url))

}#END buildURLoscarV2




buildURLoscar.old <- function(url.path = "http://podaac-opendap.jpl.nasa.gov:80/opendap/allData/oscar/preview/L4/oscar_third_deg/", filename.prefix = "oscar_vel", file.prefix, filename.suffix = ".nc.gz.ascii?", time.length ){

  vars.df <- data.frame(matrix(ncol = 4, byrow=TRUE,
                               c("TRUE", "maps", "time", paste0("0:1:", time.length-1),
                                 "TRUE", "maps", "depth", "0:1:0",
                                 "TRUE", "maps", "latitude", "60:1:120",
                                 "TRUE", "maps", "longitude", "480:1:660",
                                 "TRUE", "array", "u", NA,
                                 "TRUE", "array", "v", NA
                               )
  ), stringsAsFactors = FALSE)
  colnames(vars.df) <- c("include", "var.type", "var.name", "var.index")

  var.indices <- apply(vars.df, 1, FUN = function(x, vars.df){
    if(x['include']==TRUE & x['var.type']=="maps") {
      paste0(x['var.name'], "[", x['var.index'], "],")
    }else if(x['include']==TRUE & x['var.type']=="array"){
      var.index.tmp <- vars.df$var.index[vars.df$include==TRUE & vars.df$var.type=="maps"]
      index.combined <- paste0("[", var.index.tmp , "]", collapse = "")
      paste0(x['var.name'], index.combined, ",")

    }

  }, vars.df)
  var.indices <- paste(var.indices, collapse = "")

  #remove trailing comma if it exists:
  if(substr(var.indices, nchar(var.indices), nchar(var.indices))==",") var.indices <- substr(var.indices, 1, nchar(var.indices)-1)

  url.str <- paste0(url.path, filename.prefix, file.prefix, filename.suffix, var.indices   )
  return(url.str)

}#END buildURLoscar




#' Calculate averages in an array given a second array of grouping values
#'
#' @param arr Array
#' @param arr.group
#' @param labels
#'
#' @return
#' @export
#'
#' @examples
calcArraymean <- function(arr, arr.group, labels=NULL){

  data.tmp <- lapply(split(arr, arr.group), mean, na.rm=TRUE)
  data.tmp <- data.frame(do.call(rbind, data.tmp))

  colnames(data.tmp) <- "value"

  if(!is.null(labels)) row.names(data.tmp) <- labels
  data.tmp$period.group <- row.names(data.tmp)
  data.tmp[,c("yearperiod", "polygon")] <- do.call(rbind, strsplit(data.tmp$period.group, split = ":"))
  data.tmp$polygon <- as.integer(data.tmp$polygon)
  data.tmp[,c("year", "period")] <- as.integer(do.call(rbind, strsplit(data.tmp$yearperiod, split = "-")))
  data.tmp <- data.tmp[data.tmp$polygon !=0,]
  data.tmp.wide <- data.tmp

  data.tmp.long <- data.tmp
  # colnames(data.tmp.long)[colnames(data.tmp.long)==variable] <- "value"
  # data.tmp.long$variable <- variable
  list(data.wide=data.tmp.wide, data.long=data.tmp.long)
}#END calcArraymean#END calcArraymean





#' calcgridgroup
#'
#' @param x
#' @param group
#'
#' @return
#' @export
#'
#' @examples
calcgridgroup <- function(x, group=3){
  x.min <- min(x)
  x-(x %% x.min) %% group

}#END calcgridgroup


#' calcGridmean
#'
#' @param x
#' @param window
#'
#' @return
#' @export
#'
#' @examples
calcGridmean <- function(x, window=21){

  na.n <- (window-1)/2
  mat.na <- matrix(NA, nrow=na.n, ncol = ncol(x))
  x <- rbind(mat.na, x, mat.na)
  mat.na <- matrix(NA, nrow=nrow(x), ncol = na.n)
  x <- cbind(mat.na, x, mat.na)

  x2 <- roll_mean(x,window, na.rm = TRUE)
  x2.t <- t(x2)
  x3 <- roll_mean(x2.t,window, na.rm = T)
  x3.t <- t(x3)
  x3.t
}

calcPeriodicmean.old <- function(x, year.period){
  #x is array of u or v
  out <- apply(x, c(1:2), by, year.period, mean, na.rm=TRUE)
  out <- aperm(out, c(2,3,1))
  out
}


#' calcPeriodicmean
#'
#' @param x
#' @param period
#'
#' @return
#' @export
#'
#' @examples
calcPeriodicmean <- function(x, period){
  #this is much faster
  x.t <- aperm(x, perm = c(3,1,2))

  output <- by(x.t, list(period), function(x.t2){
    rows.n <- nrow(x.t2)
    x.t3 <- as.matrix(x.t2)
    x.t3 <- array(x.t3, dim = c(rows.n, dim(x.t)[2:3]))
    x.t3 <- aperm(x.t3, c(2,3,1))
    rowMeans(x.t3,dims = 2, na.rm = TRUE)
  })

  output <- abind::abind(output, along = 3)
  dimnames(output)[1:2] <- dimnames(x)[1:2]
  output
}#END calcPeriodicmean


#' defineGridgroups
#'
#' @param lat
#' @param lon
#' @param period
#' @param polys
#'
#' @return
#' @export
#'
#' @examples
defineGridgroups <- function(lat, lon, period, polys, includeBdry=NULL){

  data.gridgroup <- expand.grid(Y=lat, X=lon)
  data.gridgroup$EID <- 1:nrow(data.gridgroup)
  data.gridgroup <- PBSmapping::as.EventData(data.gridgroup, projection=1)
  polys <- PBSmapping::as.PolySet(polys, projection=1)
  res <- PBSmapping::findPolys(data.gridgroup, polys, includeBdry = includeBdry)
  data.gridgroup <- merge(data.gridgroup, res, by="EID", all.x = TRUE)
  #data.gridgroup$PID[is.na(data.gridgroup$PID) & data.gridgroup$X>=-150] <- 4
  data.gridgroup$PID[is.na(data.gridgroup$PID)] <- 0

  gridgroup <- paste(rep(period, each=nrow(data.gridgroup)), data.gridgroup$PID, sep = ":")

  gridgroup.f <- as.factor(gridgroup)
  gridgroup.index <- as.integer(gridgroup.f)

  data.gridgroup.arr <- array(gridgroup.index, dim =  c(length(lat),length(lon), length(period)))
  dimnames(data.gridgroup.arr) <- list(lat, lon, period)

  list(gridgroup=sort(unique(gridgroup)), data.gridgroup.arr=data.gridgroup.arr)

}#END defineGridgroups




#' Download OSCAR v2 NC format daily files
#'
#' @param files.cloud
#' @param auth
#'
#' @return
#' @export
#'
#' @examples
downloadOSCARv2 <- function(files.cloud, auth=NULL){

  response <- lapply(files.cloud, function(x){
    output <- list()
    write.filename <- x$filename
    cat("\n", write.filename)

    status_code <- 404
    attempts <- 0
    #rarely the first download attempt fails, this loop allows for up to 5 retries:
    while(status_code!=200 & attempts<=5){
      attempts <- attempts+1
      if(is.null(auth)){
        response <- httr::GET(x$opendap.url, httr::write_disk(path = write.filename , overwrite = TRUE))
      }else{
        cat("\nsending pw")
        response <- httr::GET(x$opendap.url, httr::authenticate(auth$id,auth$pw),httr::write_disk(path = write.filename , overwrite = TRUE))
      }

      output$response
      status_code <- response$status_code
    }

    if(status_code !=200) {
      cat("  failed after 5 attempts")
      output$failed <- x
    }

    output
  })#end lapply

  response

}#END downloadOSCARv2





#' get_time.length
#'
#' @param file.prefix
#'
#' @return
#' @export
#'
#' @examples
get_time.length <- function(filename.prefix){
  url.path <- "https://podaac-opendap.jpl.nasa.gov/opendap/allData/oscar/preview/L4/oscar_third_deg"
  #filename.prefix <- "oscar_vel"

  filename.suffix <- ".nc.gz.dds"

  #filename <- paste0(filename.prefix, file.prefix, filename.suffix)
  filename <- paste0(filename.prefix, filename.suffix)
  url.pathname <- paste(url.path, filename, sep="/")
  metadata.filename <-  paste0("oscar.metadata", filename.prefix, ".txt")
  download.file(url.pathname, destfile =metadata.filename)

  metadata <- scan(file = metadata.filename, what="", sep="\n")
  metadata <- trimws(metadata)

  search.ind.start <- unlist(gregexpr(pattern = "time = ", metadata[2]))
  search.ind.end <- unlist(gregexpr(pattern = "]", metadata[2]))
  time.length <- as.integer(substr(x = metadata[2], search.ind.start + 6,search.ind.end -1))
  file.remove(metadata.filename)
  return(time.length)

}#END get_time.length



#' listOSCARfiles (5 day files - no longer used)
#'
#' @param url.str
#'
#' @return
#' @export
#'
#' @examples
listOSCARfiles <- function(url.str="https://podaac-opendap.jpl.nasa.gov/opendap/allData/oscar/preview/L4/oscar_third_deg/", filename.pattern="oscar_vel.*dds$"){

  doc <- XML::htmlParse(readLines(url.str), asText=TRUE)
  links <- XML::xpathSApply(doc, "//a/@href")
  XML::free(doc)

  filename <- links[grepl(filename.pattern, links)]
  filename.prefix <- removeExtensions(filename)
  days <- as.numeric(gsub("oscar_vel", replacement = "", x = filename.prefix))

  #data frame of dates available on website:
  files.df <- calcDayintervals(days)
  #Values <7001 in filename will represent a year, not a 5 day interval, so correct year using the day int value:
  files.df$year[files.df$days.int<7001] <- files.df$days.int[files.df$days.int<7001]
  files.df[files.df$days.int<7001, c("month", "date.val")] <- NA
  files.df$filename <- filename
  files.df$filename.prefix <- filename.prefix
  files.df$filename.destination <- paste0(files.df$filename.prefix, ".txt")
  files.df <- files.df[order(files.df$days.int),]

  # wanted <- links[grepl("oscar_vel", links)]
  # start.ind <- unlist(gregexpr("oscar_vel", text = wanted))
  # stop.ind <- unlist(gregexpr("\\.nc", text = wanted))

  # start.ind <- regexpr("oscar_vel", text = wanted)
  # stop.ind <- regexpr("\\.nc", text = wanted)
  # file.prefix.available <- as.integer(substr(wanted, start.ind+9, stop.ind-1))
  #file.prefix.available <- sort(unique(as.integer(file.prefix.available)))

  return(files.df)

}#END listOSCARfiles




#' listOSCARv2file
#'
#' @param url.str
#' @param dates
#'
#' @details The webpage documenting the three versions (final, interim, near real time) is https://podaac.jpl.nasa.gov/cloud-datasets?ids=Keywords%3AProjects&values=Oceans%3AOcean%20Circulation%3AOcean%20Currents%3A%3AOSCAR&view=list
#'
#'
#' @return
#' @export
#'
#' @examples
listOSCARv2file <- function(url.str="https://cmr.earthdata.nasa.gov/virtual-directory/collections/C2098858642-POCLOUD/temporal", dates = seq(as.Date("1993-01-01"), Sys.Date(), by="day")){


  dates <- gsub("-", "/", dates)
  url.base <- dirname(url.str)

  output <- lapply(dates, function(date){
       url <- paste(url.str, date, sep="/")
       cat("\n", url)
       doc <- XML::htmlParse(readLines(url), asText=TRUE)
       links <- XML::xpathSApply(doc, "//a/@href")
       XML::free(doc)
       filename <- basename(links[grep("\\.nc$",links)])
       if(length(filename)>0) list(date=date, url.base=url.base, url=url, filename=filename)
  })

  #exclude null results
  output <- output[unlist(lapply(output, function(x) !is.null(x)))]
  output

}#END listOSCARv2file





#' importOSCARv2
#'
#' @param filenames.nc A vector
#'
#' @return
#' @export
#'
#' @examples
importOSCARv2 <- function(filenames.nc){

  data.nc.list <- lapply(filenames.nc, function(filename){

    output <- list()
    nc <- ncdf4::nc_open(filename)
    nc_atts <- ncdf4::ncatt_get(nc, 0)
    output$date <- as.Date(nc_atts$time_coverage_start,  tz = "UTC")
    var.names <- attributes(nc$var)$names

    data.nc <- lapply(var.names, function(var.name){ncdf4::ncvar_get(nc, var.name)})
    names(data.nc) <- var.names

    output$lat <- data.nc$lat
    output$lon <- data.nc$lon
    output$currents <- data.nc[c("u", "v")]

    ncdf4::nc_close(nc)
    output
  })

  names(data.nc.list) <- basename(filenames.nc)
  data.nc.list

}#END importOSCARv2


#' makearray
#'
#' @param x
#' @param dimnames
#'
#' @return
#' @export
#'
#' @examples
makearray <- function(x, dimnames=NULL){

  matnames <- unique(unlist(lapply(x, function(x.elem) names(x.elem))))

  output <- lapply(matnames, function(mat){
    data.arr <- abind::abind(lapply(x, "[[", mat), along=3)
    dimnames(data.arr) <- dimnames
    data.arr
  })

  names(output) <- matnames
  output

}#END makearray



#' reviseGridPolygons
#'
#' @param polys Data frame.  The example shows how to build the default set of polygons.
#' @param azimuth Numeric. Degrees on the true north scale. Default is 325.
#' @param tangent.angle Numeric. Angle of (perpendicular) line away from coast
#'   for each polygon. Default is 90 (degrees).
#' @param tangent.length Numeric. Distance of perpendicular line away from coast
#'   for each polygon. Default is 150 (km).
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' # define polygons for grid averaging
#' #the POS values start in the NE corner of each polygon and progress clockwise
#' polys <- structure(list(PID = c(1L, 1L, 1L, 1L, 2L, 2L, 2L, 2L, 3L, 3L, 3L, 3L), POS = c(1L, 2L, 3L, 4L, 1L, 2L, 3L, 4L, 1L, 2L, 3L, 4L), Y = c(60, 54.25, 53.5, 60, 54, 50.75, 50, 53.5, 50.5, 48, 47.25, 50), X = c(-137.5, -133, -134.25, -140, -133, -128.25, -130, -134.25, -128.25, -124, -125.5, -129.5), revised = c(TRUE, FALSE, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE)), class = "data.frame", row.names = c(NA, -12L))
#' polys.list <- reviseGridPolygons(polys)
#' }
reviseGridPolygons <- function(polys, azimuth=325, tangent.angle=90, tangent.length=150){

  b <- geosphere::bearing(polys[polys$PID==1 & polys$POS==2, c("X", "Y")], polys[polys$PID==1 & polys$POS==1, c("X", "Y")])
  course <- (b + 360) %% 360
  polys[polys$PID==1 & polys$POS==1, c("X", "Y")] <- geosphere::destPoint(polys[polys$PID==1 & polys$POS==2, c("X", "Y")], b = course, d=500*1000)

  d <- tangent.length*1000
  polys[polys$PID==1 & polys$POS==4, c("X", "Y")] <- geosphere::destPoint(polys[polys$PID==1 & polys$POS==1, c("X", "Y")], b = course-tangent.angle, d=d)
  polys[polys$PID==1 & polys$POS==3, c("X", "Y")] <- geosphere::destPoint(polys[polys$PID==1 & polys$POS==2, c("X", "Y")], b = azimuth-tangent.angle, d=d)
  polys[polys$PID==2 & polys$POS==3, c("X", "Y")] <- geosphere::destPoint(polys[polys$PID==2 & polys$POS==2, c("X", "Y")], b = azimuth-tangent.angle, d=d)
  polys[polys$PID==3 & polys$POS==3, c("X", "Y")] <- geosphere::destPoint(polys[polys$PID==3 & polys$POS==2, c("X", "Y")], b = azimuth-tangent.angle, d=d)

  polys[polys$PID==2 & polys$POS==1, c("X", "Y")] <- polys[polys$PID==1 & polys$POS==2, c("X", "Y")]
  polys[polys$PID==2 & polys$POS==4, c("X", "Y")] <- polys[polys$PID==1 & polys$POS==3, c("X", "Y")]
  polys[polys$PID==3 & polys$POS==1, c("X", "Y")] <- polys[polys$PID==2 & polys$POS==2, c("X", "Y")]
  polys[polys$PID==3 & polys$POS==4, c("X", "Y")] <- polys[polys$PID==2 & polys$POS==3, c("X", "Y")]




  polys <- PBSmapping::as.PolySet(polys, projection=1)
  # polys$lat <- polys$Y
  # polys$lon <- polys$X
  polys[, c("var.v", "var.u")] <- NA

  revised.df <- read.table(header = TRUE, text="
PID POS
1 1
1 3
1 3
2 3
3 3
2 1
2 4
3 1
3 4")

  for(i in 1:nrow(revised.df)){
    polys$revised[polys$PID==revised.df$PID[i] & polys$POS==revised.df$POS[i]] <- TRUE
  }




  # distHaversine(polys[polys$PID==1 & polys$POS==1, c("X", "Y")], polys[polys$PID==1 & polys$POS==2, c("X", "Y")])/1000
  # distHaversine(polys[polys$PID==1 & polys$POS==4, c("X", "Y")], polys[polys$PID==1 & polys$POS==3, c("X", "Y")])/1000
  #
  # distHaversine(polys[polys$PID==2 & polys$POS==1, c("X", "Y")], polys[polys$PID==2 & polys$POS==2, c("X", "Y")])/1000
  # distHaversine(polys[polys$PID==2 & polys$POS==4, c("X", "Y")], polys[polys$PID==2 & polys$POS==3, c("X", "Y")])/1000
  #
  # distHaversine(polys[polys$PID==3 & polys$POS==1, c("X", "Y")], polys[polys$PID==3 & polys$POS==2, c("X", "Y")])/1000
  # distHaversine(polys[polys$PID==3 & polys$POS==4, c("X", "Y")], polys[polys$PID==3 & polys$POS==3, c("X", "Y")])/1000


  #square km of each poly:
  poly.areas <- do.call(rbind, by(as.data.frame(polys),list(polys$PID), function(x){data.frame(PID=unique(x$PID), area.kmsq=geosphere::areaPolygon(x[,c("X", "Y")])/(1000^2))}))

  list(polys=polys, poly.areas=poly.areas)


}#END reviseGridPolygons



#' remove_metadatafiles
#'
#' @param file.prefix
#'
#' @return
#' @export
#'
#' @examples
remove_metadatafiles <- function(file.prefix){

  metadata.filename <-  paste0("oscar.metadata", file.prefix, ".txt")
  if(file.exists(metadata.filename)) file.remove(metadata.filename)

}#END remove_metadatafiles




#' read_OSCARascii
#'
#' @param filename
#'
#' @return
#' @export
#'
#' @examples
read_OSCARascii <- function(filename){
  oscar.tmp <- readLines(filename)

  #metadata:
  dat.list <- lapply(oscar.tmp[2:5], function(x){
    x <- gsub(pattern = " ",replacement = "", x)
    x <- strsplit(x,  split = ",")
    var.name <- x[[1]][1]

    dat.list <- list(as.single(x[[1]][-1]))
    names(dat.list) <- var.name
    return(dat.list)
  })


  dat.list2 <- lapply(dat.list, "[[", 1)
  names(dat.list2) <- sapply(dat.list, names)



  subset.index <- grep(pattern = "^u.u|^v.v", oscar.tmp)

  #the main data:
  dat.list <- lapply(oscar.tmp[subset.index], function(x){
    x <- gsub(pattern = " ",replacement = "", x)
    x <- strsplit(x,  split = ",")

    var.aggregate <- x[[1]][1]
    lbracket.index <- unlist(gregexpr(pattern = "\\[", var.aggregate))
    equal.index <- unlist(gregexpr(pattern = "=", var.aggregate))
    rbracket.index <- unlist(gregexpr(pattern = "\\]", var.aggregate))
    index.df <- data.frame(lbracket.index=lbracket.index, equal.index= equal.index, rbracket.index=rbracket.index)

    metavar.nam <- apply(index.df,1, function(x, var.aggregate) {

      var.names <- substr(var.aggregate, x['lbracket.index']+3, x['equal.index']-1)
      var.values <- as.numeric(substr(var.aggregate, x['equal.index']+1, x['rbracket.index']-1))

      var.df <- data.frame(matrix(var.values, ncol = length(var.values), byrow = TRUE))
      colnames(var.df) <- var.names
      return(var.df)
    }, var.aggregate)
    metavar.nam <- c(metavar.nam, list(data.frame(data.type=substr(x[[1]][1],1,1), stringsAsFactors = FALSE)))
    metadat.df <- do.call('cbind', metavar.nam)

    data.vec <- as.single(x[[1]][-1])


    dat.df2 <- data.frame(matrix(data.vec, ncol=length(data.vec), byrow = TRUE ))

    dat.df <- data.frame(metadat.df, dat.df2)

    return(dat.df)
  })

  dat.df <- do.call('rbind', dat.list)

  colnames(dat.df)[seq(5, by=1, len=length(dat.list2$longitude))] <- paste0("longitude", dat.list2$longitude)

  #the time varible is days since Oct 5, 1992
  day.start <- "1992-10-5"
  day.start <- as.Date(x = day.start,  format = "%Y-%m-%d")
  dat.df$date.val <- day.start + dat.df$time
  dat.df$year <- as.integer(format(dat.df$date.val, "%Y"))
  dat.df$month <- as.integer(format(dat.df$date.val, "%m"))

  return(dat.df)


}#END read_OSCARascii



#' read_OSCARascii_array
#'
#' @param filename
#'
#' @return
#' @export
#'
#' @examples
read_OSCARascii_array <- function(filename){
  #browser()
  oscar.tmp <- readLines(filename)
  output <- list()
  output$filepath <- filename
  output$filename <- basename(filename)

  #metadata:
  dat.list <- lapply(oscar.tmp[2:5], function(x){
    x <- gsub(pattern = " ",replacement = "", x)
    x <- strsplit(x,  split = ",")
    var.name <- x[[1]][1]

    dat.list <- list(as.single(x[[1]][-1]))
    names(dat.list) <- var.name

    return(dat.list)
  })

  dat.list2 <- lapply(dat.list, "[[", 1)
  names(dat.list2) <- sapply(dat.list, names)
  output <- c(output, dat.list2)

  subset.index <- lapply(c("^u.u", "^v.v"), function(x, oscar.tmp) grep(pattern = x, oscar.tmp), oscar.tmp)
  names(subset.index) <- c('u', 'v')

  #the main data:
  currents <- lapply(names(subset.index), function(x,subset.index, filename, dat.list2){
    ncols <- length(dat.list2$longitude)
    nrows <- length(dat.list2$latitude)
    arr.depth <- length(dat.list2$time)
    oscar.tmp <- read.table(file = filename, sep = ",", colClasses = c("NULL", rep("numeric", ncols )), header = FALSE, nrows = length(subset.index[[x]]), skip = min(subset.index[[x]])-1 )

    #tmp arr rows=need cols
    #tmp arr cols= needed rows
    #tmp arr depth= needed depth
    oscar.tmp <- t(oscar.tmp)

    oscar.tmp.arr <- array(oscar.tmp, dim = c(ncols, nrows, arr.depth))

    oscar.tmp.arr <- aperm(oscar.tmp.arr, perm = c(2,1,3))

    return(oscar.tmp.arr)
  }, subset.index, filename, dat.list2)

  names(currents) <- names(subset.index)
  time.df <- calcDayintervals( dat.list2$time)
  output$time.df <- time.df

  year.month <- paste(time.df$year, sprintf("%02.0f",time.df$month), sep="-")
  output$year.month <- year.month
  output$currents <- currents

  #dat.list <- list(longitude= dat.list2$longitude, latitude= dat.list2$latitude, time= time.df, year.month= year.month, currents = currents)
  #dat.list <- list(longitude= dat.list2$longitude, latitude= dat.list2$latitude, time= time.df, year.month= time.df$year+ time.df$month/100, currents = currents)

  return(output)


}#END read_OSCARascii_array




#' reshape_OSCARlong
#'
#' @param dat
#'
#' @return
#' @export
#'
#' @examples
reshape_OSCARlong <- function(dat){

  longitude.index <- grep( pattern = "longitude", colnames(dat))
  dat.long <- reshape(data =dat, direction = "long", varying = list(longitude.index), v.names = 'value', timevar = 'longitude', times = colnames(dat)[longitude.index])
  dat.long <- subset(x = dat.long, select = -id)

  dat.long$longitude <- substr(dat.long$longitude, 10, nchar(dat.long$longitude))
  return(dat.long)
}#END reshape_OSCARlong






#' subsetToDegree
#'
#' @param dat.list
#'
#' @return
#' @export
#'
#' @examples
subsetToDegree <- function(dat.list){
  #dat.list has 3 elements:
  #a vector of longitude values
  #a vector of latitude values
  #a list, named "currents", comprising one or more 3D arrays with dim= c(length(longitude), length(latitude), length(timeseries))

  #reduce data down to degree only values (not 1/3 degree)
  lat.vec.bydeg <- seq(max(dat.list$latitude),min(dat.list$latitude),by=-1)
  lon.vec.bydeg <- seq(min(dat.list$longitude),max(dat.list$longitude),by=1)
  #
  #next two rows prepare an array that will only include values on degree points
  datByDeg.list <- lapply(dat.list$currents, FUN = function(x){
    data.bydeg <- array(NA,c(length(lon.vec.bydeg),length(lat.vec.bydeg),dim(x)[3]))
    data.bydeg[] <-  x[which(dat.list$longitude %in% lon.vec.bydeg), which(dat.list$latitude %in% lat.vec.bydeg),]
    return(data.bydeg)
  })
  #lon.vec.bydeg <-  ifelse(lon.vec.bydeg>179,-(360-lon.vec.bydeg),lon.vec.bydeg)
  dat.list$longitude=lon.vec.bydeg
  dat.list$latitude=lat.vec.bydeg
  dat.list$currents=datByDeg.list

  return(dat.list)

}#END subsetToDegree
